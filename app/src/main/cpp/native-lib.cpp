#include <jni.h>
#include <string>
#include <android/log.h>

#if defined(HAVE_NEON) && defined(HAVE_NEON_X86)
/*
  * The latest version and instruction for NEON_2_SSE.h is at:
  *    https://github.com/intel/ARM_NEON_2_x86_SSE
  */
  #include "NEON_2_SSE.h"
#elif defined(HAVE_NEON)
#include <arm_neon.h>
#endif


extern "C" JNIEXPORT jstring JNICALL
Java_com_example_neon_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

//https://www.programmersought.com/article/64396089588/
int transposition_neon(uint8_t* src,uint8_t* dst,int w,int h)
{
#if defined(HAVE_NEON) && defined(HAVE_NEON_X86)
#elif defined(HAVE_NEON)
    uint8x8x4_t mat1;
    uint8x8x4_t mat2;
    uint8x8x2_t temp1;
    uint8x8x2_t temp2;
    uint8x8x2_t temp3;
    uint8x8x2_t temp4;
    uint16x4x4_t temp11;
    uint16x4x4_t temp12;
    uint16x4x2_t temp5;
    uint16x4x2_t temp6;
    uint16x4x2_t temp7;
    uint16x4x2_t temp8;
    uint32x2x4_t temp21;
    uint32x2x4_t temp22;
    uint32x2x2_t res1;
    uint32x2x2_t res2;
    uint32x2x2_t res3;
    uint32x2x2_t res4;

    int dw=w&7;
    int dh=h&7;
    int sw=w-dw;
    int sh=h-dh;
    int x,y;
    for(y=0;y<sh;y=y+8)
    {
        for(x=0;x<sw;x=x+8)
        {
            mat1.val[0]=vld1_u8(src+y*w+x);
            mat1.val[1]=vld1_u8(src+(y+1)*w+x);
            mat1.val[2]=vld1_u8(src+(y+2)*w+x);
            mat1.val[3]=vld1_u8(src+(y+3)*w+x);
            mat2.val[0]=vld1_u8(src+(y+4)*w+x);
            mat2.val[1]=vld1_u8(src+(y+5)*w+x);
            mat2.val[2]=vld1_u8(src+(y+6)*w+x);
            mat2.val[3]=vld1_u8(src+(y+7)*w+x);
            temp1=vtrn_u8(mat1.val[0],mat1.val[1]);
            temp2=vtrn_u8(mat1.val[2],mat1.val[3]);
            temp3=vtrn_u8(mat2.val[0],mat2.val[1]);
            temp4=vtrn_u8(mat2.val[2],mat2.val[3]);

            temp11.val[0]=vreinterpret_u16_u8(temp1.val[0]);
            temp11.val[1]=vreinterpret_u16_u8(temp1.val[1]);
            temp11.val[2]=vreinterpret_u16_u8(temp2.val[0]);
            temp11.val[3]=vreinterpret_u16_u8(temp2.val[1]);
            temp12.val[0]=vreinterpret_u16_u8(temp3.val[0]);
            temp12.val[1]=vreinterpret_u16_u8(temp3.val[1]);
            temp12.val[2]=vreinterpret_u16_u8(temp4.val[0]);
            temp12.val[3]=vreinterpret_u16_u8(temp4.val[1]);

            temp5=vtrn_u16(temp11.val[0],temp11.val[2]);
            temp6=vtrn_u16(temp11.val[1],temp11.val[3]);
            temp7=vtrn_u16(temp12.val[0],temp12.val[2]);
            temp8=vtrn_u16(temp12.val[1],temp12.val[3]);

            temp21.val[0]=vreinterpret_u32_u16(temp5.val[0]);
            temp21.val[1]=vreinterpret_u32_u16(temp5.val[1]);
            temp21.val[2]=vreinterpret_u32_u16(temp6.val[0]);
            temp21.val[3]=vreinterpret_u32_u16(temp6.val[1]);
            temp22.val[0]=vreinterpret_u32_u16(temp7.val[0]);
            temp22.val[1]=vreinterpret_u32_u16(temp7.val[1]);
            temp22.val[2]=vreinterpret_u32_u16(temp8.val[0]);
            temp22.val[3]=vreinterpret_u32_u16(temp8.val[1]);

            res1=vtrn_u32(temp21.val[0],temp22.val[0]);
            res2=vtrn_u32(temp21.val[1],temp22.val[1]);
            res3=vtrn_u32(temp21.val[2],temp22.val[2]);
            res4=vtrn_u32(temp21.val[3],temp22.val[3]);

            mat1.val[0]=vreinterpret_u8_u32(res1.val[0]);
            mat1.val[1]=vreinterpret_u8_u32(res2.val[0]);
            mat1.val[2]=vreinterpret_u8_u32(res3.val[0]);
            mat1.val[3]=vreinterpret_u8_u32(res4.val[0]);
            mat2.val[0]=vreinterpret_u8_u32(res1.val[1]);
            mat2.val[1]=vreinterpret_u8_u32(res2.val[1]);
            mat2.val[2]=vreinterpret_u8_u32(res3.val[1]);
            mat2.val[3]=vreinterpret_u8_u32(res4.val[1]);

            vst1_u8(dst+x*h+y,mat1.val[0]);
            vst1_u8(dst+(x+1)*h+y,mat1.val[1]);
            vst1_u8(dst+(x+2)*h+y,mat1.val[2]);
            vst1_u8(dst+(x+3)*h+y,mat1.val[3]);
            vst1_u8(dst+(x+4)*h+y,mat2.val[0]);
            vst1_u8(dst+(x+5)*h+y,mat2.val[1]);
            vst1_u8(dst+(x+6)*h+y,mat2.val[2]);
            vst1_u8(dst+(x+7)*h+y,mat2.val[3]);
        }
    }
    for(y=sh-1;y<h;y++)
    {
        for(x=0;x<w;x++)
            dst[x*h+y]=src[y*w+x];
    }
    for(x=sw-1;x<w;x++)
    {
        for(y=0;y<sh;y++)
        {
            dst[x*h+y]=src[y*w+x];
        }
    }
#endif
    return 0;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_neon_MainActivity_rotateImage(
        JNIEnv* env,
        jobject /* this */,
        jbyteArray out,
        jbyteArray in,
        jint width,
        jint height) {
    jbyte* content_out = (env)->GetByteArrayElements(out,NULL);
    jbyte* content_in = (env)->GetByteArrayElements(in,NULL);


    __android_log_print(ANDROID_LOG_ERROR, "NativeCode", "width(%d), height(%d), %s(%d)", width, height, __FILE__, __LINE__);
    __android_log_print(ANDROID_LOG_ERROR, "NativeCode", "out(%d), in(%d), %s(%d)", content_out[0], content_in[0], __FILE__, __LINE__);

    transposition_neon((uint8_t*)content_in, (uint8_t*)content_out, width, height);//46ms

    /*
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
            content_out[y+x*height] = content_in[y*width+x];
        }
    }
     */

    (env)->ReleaseByteArrayElements(in,content_in,0);
    (env)->ReleaseByteArrayElements(out,content_out,0);

    return 0;
}
