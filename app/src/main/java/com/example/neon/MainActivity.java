package com.example.neon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        int width = 10240;
        int height = 800;
        byte[] a = new byte[width * height];
        byte[] b = new byte[width * height];
        byte[] c = new byte[width * height];

        {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    a[y + x * height] = (byte)(x&0xf);
                    b[y + x * height] = 0;
                    c[y + x * height] = 0;
                }
            }
        }

        {
            long startTime = System.currentTimeMillis();

            //160ms, 130ms
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    b[y + x * height] = a[y * width + x];
                }
            }

            long endTime = System.currentTimeMillis();

            System.out.println("処理時間：" + (endTime - startTime) + " ms");
        }

        {
            long startTime = System.currentTimeMillis();

            //a[0] = 1;
            rotateImage(c, a, width, height);//40ms

            long endTime = System.currentTimeMillis();

            System.out.println("処理時間c++：" + (endTime - startTime) + " ms");
            System.out.println("c[0]：" + c[0]);
        }
        {
            if(Arrays.equals(b, c)) {
                System.out.println("c is b");
            }
            else {
                System.out.println("c is not b");
            }
        }


    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native int rotateImage(byte[] out, byte[] in, int width, int height);
}
